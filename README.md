# pnrc_nth_root_calculator

A simple python script that calculates the n'th root of x

random decimal errors occur with n > 5
notable example: 12th root of 4095 should be 1.9999...

the long note:
this script is currently extremely messy due to the hunt for the random decimal errors.
most of the code is for debugging, and at this point I'm starting to think the error is in pythons built in POW() module
the formula used in this script was something I derived by accident through an observation of patterns on how to 
calculate square, cube and 4th roots by hand. this was later confirmed somewhere else, so it's not a mathemagical
breakthrough of any kind. my observation was simply that construction of partial solutions was similar to pascal's triangle.
I then derived the formula for 5th roots and happened on the correct answer in the first attempt. same with 6th roots.
at this point, the formulas were hardcoded, and I started working on a generic formula for any n.
