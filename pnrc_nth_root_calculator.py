#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file.  As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------
#
# This script calculates the n'th root of x.
# example usage: /path/to/pnrc_nth_root_generator.py 2 2 30 will produce
# the square root of 2 with 30 decimals.
# written and tested on Ubuntu Disco using Python 3.7.3
#
# STATUS 2020.01.31: currently random decimal errors with n greater than 5.
# notable example(s) include:
# the 12th root of 4095 (should produce 1.999xx but produces 1.812
#

import sys, os
from decimal import Decimal
# import pnrc_dumpster as dump

VNAME = 'PNRC Nth Root Calculator'
VVERSION = '1.0.0'

PRECISION = 5
VERBOSE = False
RAW_OUT = False
NEXT_B = 5
FORMULA_STRING = ''
LOG = []



# TODO - general cleanup -                  done            moved dump to dumpsterfile
# TODO - implement verbose printing -       in progress     use test file to call verbose=True
# TODO - some prettyprint'ish solution -    in progress     print_expanded_solution()
# TODO - hide test output -                 done            migrated to dumpsterfile
#
# TODO - locate and fix random decimal error                keep testing stuff


class log_entry:
    def __init__(self, a, b, m, n, p, y, formula_string):
        self.a = a
        self.b = b
        self.m = m
        self.n = n
        self.p = p
        self.y = y
        self.fs = formula_string


class formula_unit:
    def __init__(self,x1,x2,x3):
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3


### square: n*(10*y)*b + b^2
### cube:   n*(10*y)^2*b + n*(10*y)*b^2 + b^3
### 4th:    n*(10*y)^3*b + 2*(n-1)*(10*y*b)^2 + n*(10*y)*b^3 + b^4
### 5th:    n*(10*y)^4*b + 3*(n-1)*(10*y)^3*b^2 + 3*(n-1)*(10*y)^2*b^3 + n*(10*y)*b^4 + b^5
### 6th:    n*(10*y)^5*b + 4*(n-1)*(10*y)^4*b^2 + 6*(n-2)*(10*y*b)^3 + 4*(n-1)*(10*y)^2*b^4 + n*(10*y)*b^5 + b^6
### 2^6 = 64   3^6 = 729   4^6 = 4096   5^6 = 15625

###         n = degree of root
###         y = solution, ignoring decimal point
###         x = radicand
###         r = remainder
###         a = next n digits of the radicand
###         b = next digit of solution
###         p: modifier for double-like product based on pascal's triangle
###         m: modifier subtracted from n, derived from half the complexity
###
###         p*(n-m)*y^i*b^j



def get_root(arg1, arg2):
    y = 0
    n = int(arg2)
    if VERBOSE: print_pascals_triangle(n)
    count = r = 0
    integer, decimals = split_float(arg1)
    mod = (n - len(integer)%n)%n
    while (((count+1)*n)-(mod+1)) < len(integer):
        low = (count*n) - mod
        if low < 0: low = 0
        high = ((count+1)*n) - (mod+1)
        if not r == 0: a = str(r)
        else: a = ""
        for i in range(low, high + 1):
            a += integer[i]
        b, total, resultset = getB(int(a), y, n)
        r = (int(a) - total)
        y = append_integer(y, b)
        count += 1
    partial_solution = str(y)
    if not r == 0: 
        for dec in range(PRECISION):
            a = str(r)
            high = (1+dec)*n -1
            low = dec*n
            for i in range(low, high + 1):
                if i < len(decimals):
                    a += decimals[i]
                else: a += '0'
            b, total, resultset = getB(int(a), y, n)
            r = int(a) - total
            y = append_integer(y, b)
        
        fix = len(str(partial_solution))
        y2 = str(y)[fix:]        
        full_solution = merge_float(partial_solution, y2)
    else: full_solution = partial_solution    
    SOLUTION = Decimal(full_solution)
    if VERBOSE: print_expanded_solution(SOLUTION, Decimal(arg1), n)
    elif not RAW_OUT: print('\nsolution: ' + str(SOLUTION) + '\n')
    return SOLUTION


def getB(a, y, n):
    b, total, resultset = _getB(a,y,n)
    global FORMULA_STRING
    FORMULA_STRING = ''
    testB(a,b,y,n)
    add_log_entry(a,b,y,n)
    return b, total, resultset


def _getB(a,y,n):
    result_B = int(NEXT_B)
    test_value, testset = testB(a,result_B,y,n)
    if test_value == a:
        return result_B, test_value, testset
    elif test_value > a:
        setNEXT_B(result_B-1)
        result_B, test_value, testset = _getB(a,y,n)
    elif test_value < a:
        inc_value, inc_set = testB(a,result_B+1,y,n)
        if inc_value <= a:
            setNEXT_B(result_B+1)
            result_B, test_value, test_set = _getB(a,y,n)
    return result_B, test_value, testset


# ERROR IN HERE SOMEWHERE
def testB(a,b,y,n):
    global FORMULA_STRING
    pascals = getPascals(n-1)
    pascal = pascals.pop()
    columns = len(pascal)
    resultset = []
    resultset.append([pow(b,n)])
    FORMULA_STRING += '  ' + str(b)+'^'+str(n)
    m = []
    for i in range(columns):
        if i <= int((columns-1)/2): m.append(i)
        else: m.append((columns-1) -i)
    for i in range(1, columns +1):
        j = n-i
        p = int(pascal.pop())
        x1 = pow(b,j)
        x2 = pow(10*y,i)
        x3 = p * (n-m[i-1])
        resultset.append([x1*x2*x3,x1,x2,x3])
        if not (i == 1) or not (i == columns): FORMULA_STRING += ' +\n'
        FORMULA_STRING += ' ' + str(b)+'^'+str(j)+' * (10*'+str(y)+')^'+str(i)+' * '+str(p)+' * ('+str(n)+'-'+str(m[i-1])+')'
    result = 0
    for res in resultset:
        result += res[0]
    return result, resultset


def add_log_entry(a,b,y,n):
    global LOG, FORMULA_STRING
    pascals = getPascals(n-1)
    pascal = pascals.pop()
    columns = len(pascal)
    m = []
    for i in range(columns):
        if i <= int((columns-1)/2): m.append(i)
        else: m.append((columns-1) -i)
    p = [int(i) for i in pascal]
    formula_strings = (FORMULA_STRING + '.')[:-1]
    entry = log_entry(a, b, m, n, p, y, formula_strings)
    LOG.append(entry)


def getPascals(n):    
    result = []
    for i in range(n-1,-1,-1):
        subresult = []
        for j in range(n-i):
            subresult.append(int(1))
        result.append(subresult)
    for row in range(1, n):
        for column in range(1, len(result[row])-1):
            a = result[row-1][column-1]
            b = result[row-1][column]
            result[row][column] = int(a) + int(b)
    return result


def power(x, n):
    result = x
    for i in range(n-1):
        result = result * result
    return result


def setPRECISION(arg):
    global PRECISION
    tmpP, dec = split_float(arg)
    if len(dec) > 0: return False
    PRECISION = int(tmpP)


def setNEXT_B(b):
    global NEXT_B
    NEXT_B = b


def merge_float(i, j):
    result = i + '.' + j
    return result


def split_float(number):
    s = str(number)
    cut = False
    integer = decimals = ""
    for c in s:
        if c is '.': cut = True
        elif not cut: integer += c #= append_integer(integer,c)
        else: decimals += c #= append_integer(decimals,c)
    return integer, decimals


def append_integer(i, j):
    return int((str(i) + str(j)))


def print_pascals_triangle(n):
    print('\n--------------------------')
    print("building pascal's triangle")
    pascals = getPascals(n-1)
    calcs = len(pascals[-1])
    w = len(str(pascals[-1][int(calcs/2)])) *2
    column_count = len(pascals[-1])
    row_count = column_count
    largest_int = len(str(pascals[-1][int(column_count/2)]))
    print_count = (column_count *2) -1
    actual_row = []
    print('\nrows:\t\t' + str(row_count),
          '\ncolumns:\t' + str(column_count),
          '\nprint count:\t' + str(print_count),
          '\n#digits:\t' + str(largest_int),
          '\n')
    for i in range(row_count):
        tmp_list = []
        for j in range(print_count):
            tmp_list.append(False)
        actual_row.append(tmp_list)
    for i in range(row_count):
        start = ((column_count +1) - len(pascals[i]) -1)
        end = (column_count -1) + len(pascals[i])
        for j in range(start, end, 2):
            actual_row[i][j] = True
    for i in range(row_count):
        for j in range(print_count):
            if actual_row[i][j]:
                tmp = int(pascals[i].pop())
                print('{0:{width}}'.format(tmp, width=largest_int), end='')
            else:
                print('{0:{width}}'.format('', width=largest_int), end='')
        print()
    m = []
    for i in range(calcs):
        if i <= int((calcs-1)/2): m.append(i)
        else: m.append((calcs-1) -i)
    for i in range(len(actual_row[-1])):
        if actual_row[-1][i]:
            tmpM = int(m.pop())
            print('{0:{width}}'.format(tmpM, width=largest_int), end='')
        else:
            print('{0:{width}}'.format('', width=largest_int), end='')
    print("\n\nfinished building pascal's triangle")
    print('-----------------------------------\n')

"""
def abort_and_dump():
    if VERBOSE: dump.expanded_info(LOG)
    else: dump.simple_info(LOG)
    sys.exit()
"""

def print_help():
    print('1st argument:\tnumeric value from which to calculate the nth root')
    print('2nd argument:\tthe n value')
    print('3rd argument:\t(optional) target number of digits to calculate (default = 3)\n')

def hotfix(integer,width,position,length):
    result = ''
    if position == 'prepend' and length > width:
        result += '0'
    for i,c in enumerate(str(integer)):
        if i == int(length/width)%width and position == 'prepend':
            result += ' '
        result += c
        if i == length%width and position == 'append':
            result += ' '
    if position == 'append':
        while length >= len(result):
            if len(result)%width == 0: result += ' '
            result += '0'

    return result
# and length <= len(str(integer)): 

# y = x ^ n
# TODO - complete prettyprint'ish solution
def print_expanded_solution(y,x,n):
    y_int, y_dec = split_float(y)
    x_int, x_dec = split_float(x)
    blockwidth = n+1
    indentation = len(str(n))+4

    line1 = ' '*(indentation+1) + ' '.join(['{0:>{width}}'.format(i,width=n) for i in y_int]) + '.' + ' '.join(['{0:>{width}}'.format(d,width=n) for d in y_dec])
    line2 = ' '*indentation + u'\u2014'.format()*(blockwidth*(len(str(y_int))+len(str(y_dec)))+1)
    line3 = ' '*(indentation-4) + str(n) + ' /  '
    line4 = ' '*(indentation-4) + '\/   ' + hotfix(x_int,n,'prepend',(n+len(str(x_int))%n)) + '.' + hotfix(x_dec,n,'append',n*len(str(y_dec)))
    
    
    
    
    print(line1)
    print(line2)
    print(line3)
    print(line4)
    print()
#    for entry in LOG:
#        print()
#        print(entry.a, entry.b, entry.m, entry.p, entry.y)
#        print(entry.fs)

def calculate_root(arg1, arg2,verbose=False,precision=3,raw_out=False):
    global VERBOSE, PRECISION, RAW_OUT
    VERBOSE = verbose
    PRECISION = precision
    RAW_OUT = raw_out
    return get_root(str(arg1), str(arg2))


def main(args):
    if len(args) == 3: setPRECISION(args[2])
    get_root(str(args[0]), str(args[1]))


if __name__ == '__main__':
    print(VNAME, 'version', VVERSION)
    if (len(sys.argv) == 1) or (len(sys.argv) > 4): 
        print_help()
        exit()
    main(sys.argv[1:])
    exit()

