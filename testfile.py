#!/usr/bin/python3
# -*- coding: utf-8 -*-

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file.  As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------
#
# This script is a testfile for pnrc_nth_root_generator.py
# use as inspiration, at own peril
# written and tested on Ubuntu Disco using Python 3.7.3

import pnrc_nth_root_calculator as calc


def sqrt_2():
    print('square root of 2, default values:')
    calc.calculate_root(2,2)
    print('square root of 2, 10 digits:')
    calc.calculate_root(2,2,precision=10)
    print('square root of 2, 10 digits, verbose output:')
    calc.calculate_root(2,2,precision=10,verbose=True)


def do_12th_roots():
    print("12'th root of 4096:")
    calc.calculate_root(4096,12,precision=5)
    print("12'th root of 4097, 5 digits:")
    calc.calculate_root(4097,12,precision=5)
    print("12'th root of 4095, 5 digits, verbose:")
    calc.calculate_root(4095,12,precision=5,verbose=True)

def work_4095():
#    calc.calculate_root(4095,12,precision=2,verbose=True)
    calc.calculate_root(4095,6,precision=2,verbose=True)
#    calc.calculate_root(4095,8,precision=5,verbose=True)
    calc.calculate_root(2,2,precision=2,verbose=True)
    calc.calculate_root(152.2756,2,precision=3,verbose=True)

def print_triangles():
    calc.print_pascals_triangle(17)
#    for i in range(2,18):
#        calc.print_pascals_triangle(i)

def make_data():
    filename = 'full_list_n9.txt'
    with open(filename, 'w') as testfile:
        for i in range(2,5001):
            testfile.write(str(calc.calculate_root(i,9,precision=10,raw_out=True)))
            testfile.write('\n')
        testfile.close()

def handle_data():
    filename = 'test_output.txt'
    with open(filename) as testfile:
        data = testfile.read().splitlines()
        last_result = 1
        for i, line in enumerate(data):
            current_result = line.strip()
#            print(last_result, current_result)
            if float(last_result) > float(current_result):
                output_error(i, last_result, current_result)
            last_result = current_result
        testfile.close()

def output_error(i, last, current):
    val = i+1
    print('last result:\t{_last}\tfor i-value:\t{_val}'.format(_last=last,_val=val))
    val += 1
    print('current result:\t{_current}\tfor i-value:\t{_val}'.format(_current=current,_val=val))

def main():
#    sqrt_2()
#    do_12th_roots()
#    work_4095()
#    print_triangles()
    make_data()
#    handle_data()

if __name__ == '__main__':
    main()
    exit()
